
/*
 * set up JQuery-UI date widgets
 */
$(function() {
  $( "#datefrom" ).datepicker();
  $( "#dateto" ).datepicker();
});


var dischargeSources = [
    { 'id': 'all',         'descr': 'All' },
    { 'id': 'Mogden',      'descr': 'Mogden sewage treatment works' },
    { 'id': 'Hammersmith', 'descr': 'Hammersmith pumping station' }
  ];


var notificationFormats = [
    { 'id': 'em', 'descr': 'notifications' },
    { 'id': 'tw', 'descr': 'tweets' }
  ];


/*
 * log a message (to the console)
 */
function log(s) {
  if (console && console.log) {
    if (typeof console.log == 'function') {
      console.log(s);
    }
  }
}

/*
 * debug an object's properties 
 */
function objToString(o) {
    var s = o + "={";
    for (p in o) {
        s += p + "=>" + o[p] + "; ";
    }
    s += "}";
    return s;
}


/*
 * Convert date (from EU-format datepicker) to yyyy-mm-dd (compatible with the app.)
 */
function dateStrToJSON(str) {
  if (str) {
    var d = str.split('/');
    if (d.length >= 3) {
      return d[2]+"-"+d[1]+"-"+d[0];
    }
  }
  return null;
}


/*
 * NG filters
 */
var sdnFilters = angular.module('sdnFilters', []);

/*
 * filter to display notification/tweet based on selected format
 */
sdnFilters.filter('notificationMsg', [function() {
  return function(sdn, format) {
    try {
      if ( !format || format.toString().indexOf('tw') == 0 ) {
        return sdn.tweet;
      } else {
        return sdn.content;
      }
    } catch (err) {
      // typically, no tweet
      log("notificationMsg error: "+err);
    }
    return "";
  }
}]);

/*
 * filter to display tweet status if applicable
 */
sdnFilters.filter('tweetStatus', [function() {
  return function(sdn, format) {
    try {
      if ( !format || format.toString().indexOf('tw') == 0 ) {
        return " [" + sdn.tweet_status + "," + sdn.tweet.length + "]";
      }
    } catch (err) {
      // typically, no tweet
      log("notificationMsg error: "+err);
    }
    return "";
  }
}]);

/*
 * filter to display notification sender
 * Only want to see "From" if email format selected
 */
sdnFilters.filter('notificationSender', [function() {
  return function(sdn, format) {
    try {
      if ( format && format.toString().indexOf('em') == 0 ) {
        return "From: "+sdn.sender+"\n";
      }
    } catch (err) {
      log("notificationSender error: "+err);
    }
    return "";
  }
}]);

/*
 * filter to expand discharge location to full description
 */
sdnFilters.filter('locationDescription', [function() {
  return function(location) {
    var description = location;
    var sources = dischargeSources;
    for (s in sources) {
      if (sources[s].id == location) {
        description = sources[s].descr;
        break;
      }
    }
    return description;
  }
}]);


/*
 * NG query service
 * see: http://code.angularjs.org/1.2.3/docs/tutorial/step_11
 * see: http://www.masnun.com/2013/08/28/rest-access-in-angularjs-using-ngresource.html
 */
var sdnServices = angular.module('sdnServices', ['ngResource']);
sdnServices.factory('SDN', ['$resource', '$routeParams', function($resource, $routeParams) {
  return $resource(
    '/json', 
    {}, 
    {
      query: { method: 'GET', isArray: false }
    }
  );
}]);

sdnServices.factory('Paginator', function($http) {
  var Paginator = function() {
    this.items = [];
    this.busy = false;
    this.offset = 0;
  };

  Paginator.prototype.nextPage = function() {
    if (this.busy) return;
    this.busy = true;

    var url = "/json?offset=" + this.offset;
    $http.jsonp(url).success(function(data) {
      var items = data.data.children;
      for (var i = 0; i < items.length; i++) {
        this.items.push(items[i].data);
      }
      this.offset = this.items.length;
      this.busy = false;
    }.bind(this));
  };

  return Paginator;
}); 

/*
 * NG controller / scope
 */
var sdnControllers = angular.module('sdnControllers', []);
sdnControllers.controller('sdnCtrl', ['$scope', 'SDN', function($scope, SDN) {

  $scope.sources = dischargeSources;
  $scope.formats = notificationFormats;

  $scope.maxresults = 10;
  $scope.format = $scope.formats[0].id;
  $scope.source = $scope.sources[0].id;
  $scope.datefrom = null;
  $scope.dateto = null;
  $scope.lastMaxResults = 10;

  $scope.processForm = function() {
    $scope.lastMaxResults = $scope.maxresults;
    $scope.response = SDN.query( {
      'maxResults': $scope.maxresults,
      'location': $scope.source,
      'dateFrom': dateStrToJSON( $scope.datefrom ),
      'dateTo': dateStrToJSON( $scope.dateto )
    } );
  };

  $scope.changeMaxResults = function() {
    if ($scope.maxresults != $scope.lastMaxResults) {
      $scope.processForm();
    }
  }
	
  scope.paginator = new Paginator();
  
  $scope.processForm();
  
}]);


/*
 * NG top-level application
 * Sets list of dependencies.
 */
var sdnApp = angular.module('sdnApp', [
	'ngRoute', 
	'ngResource', 
	'sdnFilters', 
	'sdnControllers', 
	'sdnServices', 
	'infinite-scroll'
]);

