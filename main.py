#!/usr/bin/env python
# -*- coding: utf-8 -*-

import logging.config
import webapp2
from sdn import *

DEFAULT_LOGGING = {
	'version': 1,
	'disable_existing_loggers': False,
	'loggers': {
		'': {
			'level': 'INFO',
		}
	}
}
logging.config.dictConfig(DEFAULT_LOGGING)

# Map URLs to application functions
url_list = [
	(r'/_ah/mail/.*', NotificationEmail),
	(r'/json',        AjaxRequest),
	(r'/sdn',         SDNView),
	(r'/admin.*',     SDNAdmin),
]

# Main SDN entry point - see app.yaml
app = webapp2.WSGIApplication(
	url_list,
	debug = True)
