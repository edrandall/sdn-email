#
# Update discharge quantities
# (c) 2013 Ed Randall
#

import logging
import webapp2
import json
import datetime
import sys

from model import *
from json_local import *

	
# Handle a request to update discharge quantities
class SDNUpdate(webapp2.RequestHandler):
	def get(self):
		try:
			id = long( self.request.get('id') )
			location = self.request.get('l8n')
			amount = long( self.request.get('amount') )

			parent_key = db.Key.from_path('SDN', location)
			key = db.Key.from_path('SDN', id, parent=parent_key)
			entity = db.get(key)
			
			if (entity == None):
				entity = SDN( parent=parent_key )
				
			if (amount > 0):
				entity.discharge_volume = amount
				entity.put()
			
			values = {
				'metadata': {
					'key': key.id_or_name(),
					'id': id,
					'amount': amount,
					'now': datetime.datetime.now(TZ_LONDON).isoformat()
				},
				'notification': entity
			}
			
			self.response.headers.add_header('content-type', 'application/json', charset='utf-8')
			self.response.headers.add_header('cache-control', 'no-cache') # for IE8
			resp = json.dumps(values, cls=LocalJSONEncoder, indent=2)
			self.response.out.write(resp) 
			
		except Exception as ex:
			self.response.out.write( ex )
