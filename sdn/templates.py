
import jinja2

# Configure template location
templates = jinja2.Environment(
	loader = jinja2.PackageLoader('sdn', 'templates')
)
