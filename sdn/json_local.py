
import json

class LocalJSONEncoder(json.JSONEncoder):
	def default(self, obj):
		if hasattr(obj, 'isoformat'):
			return obj.isoformat()
		if hasattr(obj, 'toJSONable'):
			return obj.toJSONable()
		raise TypeError, 'Object of type %s with value of %s is not JSON serializable' % (type(obj), repr(obj))
