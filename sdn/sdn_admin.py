#
# Admin request handler
# (c) 2014 Ed Randall
#

import logging
import urllib
import webapp2
import datetime

from google.appengine.api import users

from model import *
from location import *
from templates import *


# Admin request handler
class SDNAdmin(webapp2.RequestHandler):
	def get(self):
		try:
			if (not(users.is_current_user_admin())):
				logging.info("Not an admin user")
				self.response.write("Not an admin user") 
				return

			path = self.request.path
			logging.info("path="+path)
			if (path == '/admin/setup'):
				Location.setup()
				self.admin()

			elif (path == '/admin/update'):
				self.admin

			else:
				self.admin()

		except Exception as ex:
			logging.error("Admin error: "+str(ex))
			self.response.out.write("Admin error: "+str(ex)) 


	def admin(self):
		num = self.request.get('maxresults')
		try: 
			num = int(num)
			if num <= 0: num = 10
		except: 
			num = 10
		
		query = Location.all().order('name')
		locations = query.fetch(None)

		query = SDN.all().order('-date')
		notifications = query.fetch(num)
		for sdn in notifications:
			sdn.localdate = TZ_UTC.localize(sdn.date).astimezone(TZ_LONDON)
		
		template_values = {
			'now': datetime.datetime.now(TZ_LONDON),
			'num': num,
			'notifications': notifications,
			'locations': locations
		}

		template = templates.get_template('admin.html')
		self.response.out.write(template.render(template_values))
