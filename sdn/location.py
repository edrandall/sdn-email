
import json
import logging
import re

from google.appengine.ext import db

	
# Models a discharge site location
class Location(db.Model):
	name = db.StringProperty(indexed=True)
	description = db.StringProperty(indexed=False)
	lat = db.FloatProperty(indexed=False)
	long = db.FloatProperty(indexed=False)
		
	def getName(self):
		return self.name
		
	def hasLatLong(self):
		return True
		
	def getLatitude(self):
		return self.lat
		
	def getLongitude(self):
		return self.long
	
	def toJSONable(self):
		return {
			'id': self.key().id_or_name(),
			'name': self.name,
			'description': self.description,
			'latitude': self.lat,
			'longitude': self.long,
		}

	@staticmethod
	def parse(subject):
		location = None
		locations = Location.all().fetch(None)
		for loc in locations:
			name = loc.name
			if (re.search(name, subject, re.IGNORECASE) != None):
				location = loc
				break
		return location

	@staticmethod
	def setup():
		locations = [
				{ 'name': 'Mogden',      'descr':'Mogden sewage treatment works', 'lat':51.468512, 'long':-0.319870 },
				{ 'name': 'Hammersmith', 'descr':'Hammersmith pumping station',   'lat':51.487984, 'long':-0.229983 },
				{ 'name': 'Lots Road',   'descr':'Lots Road pumping station',     'lat':51.478638, 'long':-0.178564 },
				{ 'name': 'Western',     'descr':'Western pumping station',       'lat':51.485489, 'long':-0.148127 },
			]
			
		for loc in locations:
			key = db.Key.from_path('Location', loc['name'])
			location = Location.get(key)
			if (location == None):
				location = Location(key_name=loc['name'])
				location.name = loc['name']
				location.description = loc['descr']
				location.lat = loc['lat']
				location.long = loc['long']
				location.put()
				logging.info("setup: added new location: "+location.key().id_or_name()+" => "+location.description )
			
	
