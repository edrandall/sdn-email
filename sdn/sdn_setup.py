#
# Setup  request handler
# (c) 2014 Ed Randall
#

import logging
import urllib
import webapp2

from model import *
from location import *


# Initialise any new static data 
class SDNSetup(webapp2.RequestHandler):
	def get(self):
		try:
			Location.setup()
			logging.info("Setup request completed OK")
			self.response.out.write("Setup OK") 

		except Exception as ex:
			logging.error("Setup failure: "+str(ex))
			self.response.out.write("Setup failed: "+str(ex)) 
			
		