#
# Parse content of "Rower Notification" emails from Thames Water and send to Twitter.
# (c) 2012-2013 Ed Randall
#

import cgi
import datetime
import email
import htmlentitydefs
import logging
import oauth2 as oauth
import os
import quopri
import re
import urllib
import webapp2

import ConfigParser
from HTMLParser import HTMLParser

from google.appengine.api import users
from google.appengine.ext import db
from google.appengine.ext.webapp.mail_handlers import InboundMailHandler
from google.appengine.ext.webapp.util import run_wsgi_app

from model import *
from location import *


# Log the type of an object
def logType(x):
	logging.info("type("+str(x)+")="+str(type(x)))


#
# Parse the Date field from the email to UTC
# Adjust for GMT-BST timezone
def parseRFC822Date(text):
	tuple = email.utils.parsedate_tz(text)
	timestamp = email.utils.mktime_tz( tuple )
	dt = datetime.datetime.fromtimestamp( timestamp, TZ_LONDON )
	return dt


#
# Extract text content from between HTML tags in the email
# http://stackoverflow.com/questions/328356/extracting-text-from-html-file-using-python
class HTMLTextExtractor(HTMLParser):
	def __init__(self):
		HTMLParser.__init__(self)
		self.result = [ ]
		self.hide_output = False

	def handle_starttag(self, tag, attrs):
		if tag in ('script', 'style'):
			self.hide_output = True
		elif not self.hide_output and tag in ('p', 'br', 'a'):
			self.result.append(u' ')

	def handle_endtag(self, tag):
		if tag in ('script', 'style'):
			self.hide_output = False
		elif not self.hide_output and tag in ('p', 'br', 'a'):
			self.result.append(u' ')

	def handle_startendtag(self, tag, attrs):
		if not self.hide_output and tag in ('p', 'br', 'a'):
			self.result.append(u' ')

	def handle_data(self, text):
		if text and not self.hide_output:
			text = text.replace(u'\n', u' ').strip()
			self.result.append(text)

	def handle_charref(self, number):
		if not self.hide_output:
			codepoint = int(number[1:], 16) if number[0] in (u'x', u'X') else int(number)
			if (codepoint > 31 and codepoint < 127):
				self.result.append(unichr(codepoint))

	def handle_entityref(self, name):
		if not self.hide_output:
			codepoint = htmlentitydefs.name2codepoint[name]
			if (codepoint > 31 and codepoint < 127):
				self.result.append(unichr(codepoint))

	def getText(self):
		text = u''.join(self.result)
		text = re.sub(r'\s+', u' ', text)
		return text

	def getTextArray(self):
		return self.result


# Extract array of text strings from raw HTML
def htmlToTextArray(html):
	s = HTMLTextExtractor()
	s.feed(html)
	return s.getTextArray()


# Find the line number where the given pattern occurs
def findLineMatching(pattern, msgArray):
	p = re.compile(pattern, re.I)
	return next((i for i, line in enumerate(msgArray) if p.search(line)), -1)


# Convenient text find/replace wrapper
class Replacer:
	def __init__(self, find, rep):
		self.find = find
		self.rep = rep

	def replace(self, line):
		return re.sub(self.find, self.rep, line, 0, re.I)


# Parse sewage discharge message text into a tweet
class Tweet:
	def __init__(self, config):
		self.config = dict(config)
		# consumer_key
		# consumer_secret
		# access_token
		# access_secret
		# statusupdate_url
		# website_url

	def parseToTweet(self, date, msgArray):
		pattern = r'\bdischarg.+\b(storm|sewage)\b.*\binto\b.+\bRiver\b'
		lineNum = findLineMatching(pattern, msgArray)
		if lineNum < 0:
			logging.warn("parseToTweet: findLineMatching("+pattern+") failed to find line in:\n" +'|'.join(msgArray))
			self.tweet = None
			return

		line = msgArray[lineNum]

		patterns = [
			Replacer(r'[,.]', r' '),
			Replacer(r'\sPumping\sStation', r' PS'),
			Replacer(r'\sSewage\sTreatment\sWorks', r' STW'),
			Replacer(r'\sHammersmith', r' #Hammersmith'),
			Replacer(r'\sMogden', r' #Mogden'),
			Replacer(r'\sThames', r' #Thames'),
			Replacer(r'\s?Following\s.*\srecent\srainfall\s', r''),
			Replacer(r'\sdue.*lack.*capacity.*sewer\snetwork', r' #supersewer'),
			Replacer(r'\ssewage', r' #sewage'),
			Replacer(r'Within\s', r'In '),
			Replacer(r'\shas in\s', r' In '),
			Replacer(r'\sinto\s', r' to '),
			Replacer(r'\swill be\s', r' '),
			Replacer(r'\sthe\s', r' '),
			Replacer(r'\s\s+', r' '),
			Replacer(r'^\s+(.*)', r'\1'),
			Replacer(r'(.*)\s+$', r'\1'),
		]
		for p in patterns:
			line = p.replace(line)

		if re.search(r'\s#sewage', line, re.I) == None:
			line = line + ' #sewage'

		line = 'From @Thameswater '+date.strftime('%d/%m/%y %H:%M')+' '+line+' '+self.config['website_url']
		self.tweet = line
		return

	def getTweet(self):
		return self.tweet

	def sendStatusUpdate(self, location):
		if (self.config.get('test_mode', False) != False):
			logging.debug("sendStatusUpdate: test mode - not sending tweet.")
			return 418 # I'm a teapot (RFC 2324)

		consumer = oauth.Consumer(key=self.config['consumer_key'], secret=self.config['consumer_secret'])
		token = oauth.Token(key=self.config['access_token'], secret=self.config['access_secret'])
		client = oauth.Client(consumer, token)

		# Post a status update - https://dev.twitter.com/docs/api/1.1/post/statuses/update
		resp, content = client.request(
							self.config['statusupdate_url'],
							method = 'POST',
							body = urllib.urlencode({
								'status':	self.tweet,
								'lat':		location.getLatitude(),
								'long':		location.getLongitude(),
								'display_coordinates': location.hasLatLong()
							})
						)
		logging.debug("sendStatusUpdate: response status="+str(resp.status)+" content="+str(content)+" headers="+str(resp))
		return resp.status


# Handle an incoming email
# Parse the message 
# Store the parsed message in the database
# Send a notification out on twitter
class NotificationEmail(InboundMailHandler):
	def receive(self, emsg):
		self.config = ConfigParser.SafeConfigParser()
		self.config.read('config/sdn.ini')

		logging.info("Received email message from: " +emsg.sender +" date: " +emsg.date)

		if (hasattr(emsg, 'subject') == False):
			logging.warn("No message subject.")
			return
		logging.debug("email subject: "+emsg.subject)

		# parse location from subject
		location = Location.parse(emsg.subject)
		if (location == None):
			logging.warn("Unknown location in subject: "+emsg.subject)
			return
		logging.debug("Location: "+location.getName()+" ["+str(location.getLatitude())+","+str(location.getLongitude())+"]")

		notification = SDN( parent=location.key() )
		notification.location = location.getName()
		notification.sender = emsg.sender
		notification.date = parseRFC822Date( emsg.date )

		decoded = None
		for content_type, body in emsg.bodies('text/html'):
			text = body.decode()
			text = quopri.decodestring(text)
			decoded = htmlToTextArray(text)
			break
		
		if (decoded == None):
			for content_type, body in emsg.bodies('text/plain'):
				text = body.decode()
				decoded = filter(None, text.split('\n'))
				break
		
		if (decoded == None):
			logging.warn("No readable body section in message: "+emsg.original.as_string())
			return

		# Find email message signature line
		n = findLineMatching(r'[-_]+$', decoded)
		if (n < 0):
			n = findLineMatching('Regards', decoded)
		if (n < 0):
			# Still not found, just guess length of interesting content
			n = len(decoded)-1
			if (n > 40):
				n = 40 
			logging.warn("Unable to locate signature in message: "+str(decoded))
		else:
			logging.debug("Found signature at line:"+str(n))
		logging.debug("Chopping at line:"+str(n)+":"+str(decoded[n]))
		decoded = decoded[0:n]

		# Save to database
		notification.content = db.Text(u''.join(decoded))
		notification.put()
		logging.debug("saved notification: "
			+str(notification)+";"
			+notification.sender+";"
			+notification.location+";"
			+str(notification.date)+";")

		# Prepare Twitter API
		tweet = Tweet( self.config.items('sdn') )

		# Create line of text for twitter tweet
		tweet.parseToTweet(notification.date, decoded)
		notification.tweet = tweet.getTweet()
		if (notification.tweet != None):
			# Send twitter "status update" (tweet)
			tl = len(notification.tweet)
			logging.debug("Parsed to tweet len["+str(tl)+"]: "+notification.tweet)
			status = tweet.sendStatusUpdate(location)
			logging.info("tweet: len="+str(tl)+"; status="+str(status))

			# Update database with tweet & status
			notification.tweet_status = status
			notification.put()

		else:
			logging.warn("Unable to parse to tweet.")

		return
