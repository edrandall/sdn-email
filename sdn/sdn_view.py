#
# Display notifications using "jinja2" template
# (c) 2012-2013 Ed Randall
#

import cgi
import jinja2
import logging
import os
import urllib
import webapp2

import datetime

from model import *
from templates import *

	
# Handle an HTTP request to view notifications on the web		
class SDNView(webapp2.RequestHandler):
	def get(self):
		format = self.request.get('format')
		num = self.request.get('num')
		try: 
			num = int(num)
			if num <= 0: num = 10
		except: 
			num = 10
		
		query = SDN.all().order('-date')
		notifications = query.fetch(num)
		for sdn in notifications:
			sdn.localdate = TZ_UTC.localize(sdn.date).astimezone(TZ_LONDON)
		
		template_values = {
			'now': datetime.datetime.now(TZ_LONDON),
			'format': format,
			'num': num,
			'notifications': notifications
		}

		template = templates.get_template('sdn.html')
		self.response.out.write(template.render(template_values))
