
import json
import pytz

from google.appengine.ext import db

# timezones
TZ_UTC = pytz.timezone('UTC')
TZ_LONDON = pytz.timezone('Europe/London')


# Models an individual notification with a sender, date, location and content
# see https://developers.google.com/appengine/docs/python/datastore/entities
class SDN(db.Model):
	sender = db.StringProperty(indexed=False)
	date = db.DateTimeProperty(auto_now_add=True)
	location = db.StringProperty()
	content = db.TextProperty(indexed=False)
	tweet = db.StringProperty(indexed=False)
	tweet_status = db.IntegerProperty(indexed=False)
	discharge_volume = db.IntegerProperty(indexed=False)
	
	def toJSONable(sdn):
		localdate = TZ_UTC.localize(sdn.date).astimezone(TZ_LONDON)
		return {
			'id': sdn.key().id(),
			'sender': sdn.sender,
			'date': localdate.isoformat(),
			'location': sdn.location,
			'volume': sdn.discharge_volume,
			'tweet_status': sdn.tweet_status,
			'tweet': sdn.tweet,
			'content': sdn.content
		}
		
# Create a Datastore key for an SDN entity
def sdn_key(location=None):
	return db.Key.from_path('SDN', location)
