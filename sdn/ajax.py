
import webapp2
import json
import datetime
import logging

from model import *
from json_local import *


# Handle an AJAX HTTP request producing JSON output
class AjaxRequest(webapp2.RequestHandler):
	def get(self):
		location = self.request.get('location')
		dateFrom = self.request.get('dateFrom')
		dateTo = self.request.get('dateTo')
		
		maxResults = self.request.get('maxResults')
		try: 
			maxResults = int(maxResults)
			if maxResults <= 0: maxResults = 10
		except: 
			maxResults = 10
		
		query = SDN.all()
		if (location):
			if (location != 'all'):
				query.filter('location =', location)
		if (dateFrom):
			try:
				df = datetime.datetime.strptime(dateFrom, '%Y-%m-%d')
				query.filter('date >=', df)
			except ValueError:
				logging.warn("Could not parse dateFrom: "+dateFrom);
		if (dateTo):
			try:
				dt = datetime.datetime.strptime(dateTo, '%Y-%m-%d')
				dt += datetime.timedelta(days=1)
				query.filter('date < ', dt)
			except ValueError:
				logging.warn("Could not parse dateTo: "+dateTo);
				
		query.order('-date')
		notifications = query.fetch(maxResults)
		
		values = {
			'metadata': {
				'now': datetime.datetime.now(TZ_LONDON),
				'location': location,
				'dateFrom': dateFrom,
				'dateTo': dateTo,
				'maxResults': maxResults,
				'numResults': len(notifications)
			},
			'notifications': notifications
		}
		
		self.response.headers.add_header('content-type', 'application/json', charset='utf-8')
		self.response.headers.add_header('cache-control', 'no-cache') # for IE8
		resp = json.dumps(values, cls=LocalJSONEncoder, indent=2)
		self.response.out.write(resp) 




