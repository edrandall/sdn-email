From:
Stephen Charles <stephen.charles@thameswater.co.uk>

To:
sdn@sdn-email.appspotmail.com

Cc:

Subject:
Rower Notification - Hammersmith Pumping Station

Message body (plain text): 
<html xmlns:v="urn:schemas-microsoft-com:vml" xmlns:o="urn:schemas-microsoft-com:office:office" xmlns:w="urn:schemas-microsoft-com:office:word" xmlns:m="http://schemas.microsoft.com/office/2004/12/omml" xmlns="http://www.w3.org/TR/REC-html40">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=us-ascii">
<meta name="Generator" content="Microsoft Word 14 (filtered medium)">
<style><!--
/* Font Definitions */
@font-face
	{font-family:"Tms Rmn";
	panose-1:2 2 6 3 4 5 5 2 3 4;}
@font-face
	{font-family:Helv;
	panose-1:2 11 6 4 2 2 2 3 2 4;}
@font-face
	{font-family:Helv;
	panose-1:2 11 6 4 2 2 2 3 2 4;}
@font-face
	{font-family:Calibri;
	panose-1:2 15 5 2 2 2 4 3 2 4;}
@font-face
	{font-family:Tahoma;
	panose-1:2 11 6 4 3 5 4 4 2 4;}
/* Style Definitions */
p.MsoNormal, li.MsoNormal, div.MsoNormal
	{margin:0cm;
	margin-bottom:.0001pt;
	font-size:12.0pt;
	font-family:"Times New Roman","serif";}
a:link, span.MsoHyperlink
	{mso-style-priority:99;
	color:blue;
	text-decoration:underline;}
a:visited, span.MsoHyperlinkFollowed
	{mso-style-priority:99;
	color:purple;
	text-decoration:underline;}
p
	{mso-style-priority:99;
	margin:0cm;
	margin-bottom:.0001pt;
	font-size:12.0pt;
	font-family:"Times New Roman","serif";}
span.EmailStyle18
	{mso-style-type:personal-compose;
	font-family:"Tms Rmn","serif";
	color:black;
	font-weight:bold;}
.MsoChpDefault
	{mso-style-type:export-only;
	font-size:10.0pt;}
@page WordSection1
	{size:612.0pt 792.0pt;
	margin:72.0pt 72.0pt 72.0pt 72.0pt;}
div.WordSection1
	{page:WordSection1;}
--></style><!--[if gte mso 9]><xml>
<o:shapedefaults v:ext="edit" spidmax="1026" />
</xml><![endif]--><!--[if gte mso 9]><xml>
<o:shapelayout v:ext="edit">
<o:idmap v:ext="edit" data="1" />
</o:shapelayout></xml><![endif]-->
</head>
<body lang="EN-GB" link="blue" vlink="purple">
<div class="WordSection1">
<p class="MsoNormal"><b><span style="font-size:10.0pt;font-family:&quot;Tms Rmn&quot;,&quot;serif&quot;;color:black">Rower notification from Thames Water:&nbsp; Hammersmith Pumping Station (PS)</span></b><span style="font-size:10.0pt;font-family:&quot;Tahoma&quot;,&quot;sans-serif&quot;;color:black"><o:p></o:p></span></p>
<div>
<p class="MsoNormal"><span style="font-size:10.0pt;font-family:&quot;Tahoma&quot;,&quot;sans-serif&quot;;color:black">&nbsp;<o:p></o:p></span></p>
</div>
<div>
<p class="MsoNormal"><span style="font-size:10.0pt;font-family:&quot;Tahoma&quot;,&quot;sans-serif&quot;;color:black">&nbsp;<o:p></o:p></span></p>
</div>
<div>
<p class="MsoNormal"><span style="font-size:10.0pt;font-family:&quot;Tms Rmn&quot;,&quot;serif&quot;;color:black">Following the recent rainfall, Hammersmith PS has in the last hour discharged untreated storm sewage into the River Thames, due to lack of capacity in the existing
 sewer network. </span><span style="font-size:10.0pt;font-family:&quot;Tahoma&quot;,&quot;sans-serif&quot;;color:black"><o:p></o:p></span></p>
</div>
<div>
<p class="MsoNormal"><span style="font-size:10.0pt;font-family:&quot;Tahoma&quot;,&quot;sans-serif&quot;;color:black">&nbsp;<o:p></o:p></span></p>
</div>
<div>
<p class="MsoNormal"><span style="font-size:10.0pt;font-family:&quot;Tms Rmn&quot;,&quot;serif&quot;;color:black">We are working hard to develop the Thames Tideway Tunnel to address the unacceptable volume and frequency of these discharges.
</span><span style="font-size:10.0pt;font-family:&quot;Tahoma&quot;,&quot;sans-serif&quot;;color:black"><o:p></o:p></span></p>
</div>
<div>
<p class="MsoNormal"><span style="font-size:10.0pt;font-family:&quot;Tahoma&quot;,&quot;sans-serif&quot;;color:black">&nbsp;<o:p></o:p></span></p>
</div>
<div>
<p class="MsoNormal"><span style="font-size:10.0pt;font-family:&quot;Tms Rmn&quot;,&quot;serif&quot;;color:black">The proposed tunnel would capture flows from the 34 most polluting 'combined sewer overflows' (CSOs) discharging to the river, including Hammersmith PS, and transfer
 the sewage to our works at Beckton in east London for treatment. </span><span style="font-size:10.0pt;font-family:&quot;Tahoma&quot;,&quot;sans-serif&quot;;color:black"><o:p></o:p></span></p>
</div>
<div>
<p class="MsoNormal"><span style="font-size:10.0pt;font-family:&quot;Tahoma&quot;,&quot;sans-serif&quot;;color:black">&nbsp;<o:p></o:p></span></p>
</div>
<div>
<p class="MsoNormal"><span style="font-size:10.0pt;font-family:&quot;Tms Rmn&quot;,&quot;serif&quot;;color:black">For more information view
</span><span style="font-size:11.0pt;font-family:&quot;Calibri&quot;,&quot;sans-serif&quot;;color:black"><a href="http://www.thamestidewaytunnel.co.uk" target="_blank"><span style="font-size:10.0pt;font-family:&quot;Tms Rmn&quot;,&quot;serif&quot;">www.thamestidewaytunnel.co.uk</span></a></span><span style="font-family:&quot;Helv&quot;,&quot;sans-serif&quot;;color:black">
</span><span style="font-size:10.0pt;font-family:&quot;Tahoma&quot;,&quot;sans-serif&quot;;color:black"><o:p></o:p></span></p>
</div>
<div>
<p class="MsoNormal"><span style="font-size:10.0pt;font-family:&quot;Tahoma&quot;,&quot;sans-serif&quot;;color:black">&nbsp;<o:p></o:p></span></p>
</div>
<div>
<p class="MsoNormal"><span style="font-size:10.0pt;font-family:&quot;Helv&quot;,&quot;sans-serif&quot;;color:black">Regards</span><span style="font-size:10.0pt;font-family:&quot;Tahoma&quot;,&quot;sans-serif&quot;;color:black"><o:p></o:p></span></p>
</div>
<div>
<p class="MsoNormal"><span style="font-size:10.0pt;font-family:&quot;Helv&quot;,&quot;sans-serif&quot;;color:black">Thames Water</span><span style="font-size:10.0pt;font-family:&quot;Tahoma&quot;,&quot;sans-serif&quot;;color:black"><o:p></o:p></span></p>
</div>
<div>
<p class="MsoNormal"><span style="font-size:10.0pt;font-family:&quot;Helv&quot;,&quot;sans-serif&quot;;color:black">______________________________________________________________</span><span style="font-size:10.0pt;font-family:&quot;Tahoma&quot;,&quot;sans-serif&quot;;color:black"><o:p></o:p></span></p>
</div>
<div>
<p class="MsoNormal"><span style="font-size:10.0pt;font-family:&quot;Tahoma&quot;,&quot;sans-serif&quot;;color:black">&nbsp;<o:p></o:p></span></p>
</div>
<div>
<p class="MsoNormal"><span style="font-size:10.0pt;font-family:&quot;Tahoma&quot;,&quot;sans-serif&quot;;color:black">&nbsp;<o:p></o:p></span></p>
</div>
<div>
<p class="MsoNormal"><span style="font-size:10.0pt;font-family:&quot;Tahoma&quot;,&quot;sans-serif&quot;;color:black">&nbsp;<o:p></o:p></span></p>
</div>
<div>
<p class="MsoNormal"><span style="font-size:10.0pt;font-family:&quot;Helv&quot;,&quot;sans-serif&quot;;color:black">To unregister for email alerts about discharges on the River Thames, email
</span><span style="font-size:11.0pt;font-family:&quot;Calibri&quot;,&quot;sans-serif&quot;;color:black"><a href="mailto:rower.notification@thameswater.co.uk" target="_blank"><span style="font-size:10.0pt;font-family:&quot;Helv&quot;,&quot;sans-serif&quot;">rower.notification@thameswater.co.uk</span></a></span><span style="font-size:10.0pt;font-family:&quot;Helv&quot;,&quot;sans-serif&quot;;color:black">
 with 'Discharges on the River Thames &#8211; Unregister' as the subject line. </span><span style="font-size:10.0pt;font-family:&quot;Tahoma&quot;,&quot;sans-serif&quot;;color:black"><o:p></o:p></span></p>
</div>
</div>
<br clear="both">
Did you know you can manage your account online? Pay a bill, set up a Direct Debit, change your details or even register a change of address at the click of a button, 24 hours a day. Visit http://www.thameswater.co.uk <BR>
<BR>
Thames Water Limited (company number 2366623) and Thames Water Utilities Limited (company number 2366661) are companies registered in England and Wales each with their registered office at Clearwater Court, Vastern Road, Reading, Berkshire RG1 8DB. This email is confidential and intended solely for the use of the individual to whom it is addressed. Any views or opinions presented are solely those of the author and do not necessarily represent those of Thames Water Limited or its subsidiaries. If you are not the intended recipient of this email you may not copy, use, forward or disclose its contents to any other person; please notify our Computer Service Desk on +44 (0) 203 577 8888 and destroy and delete the message and any attachments from your system. <BR>
<BR>
We provide the essential service that's at the heart of daily life, health and enjoyment.<BR>
</body>
</html>
