# README #

How to deploy and configure sdn-email

### What is this repository for? ###

* Parse Thames Water "Rower Notification" (aka Sewage Discharge Notification) emails and send out as messages on Twitter.

### How do I get set up? ###

* Designed to run on Google App Engine
* See [Google Cloud Platform - uploading your application](https://cloud.google.com/appengine/docs/python/gettingstartedpython27/uploading)
* One-off step: On your AppEngine console, create a unique app Id.
* You'll need to always edit app.yaml to refer to your own app id.
* Deployment instructions
1.  Either - use the appengine SDK "Deploy" button
1.  Or - use the command-line appcfg.py tool provided in the appengine SDK.
```
#!python

   appcfg.py update sdn-email/
```


### Contribution guidelines ###

* None

### Who do I talk to? ###

* Ed Randall